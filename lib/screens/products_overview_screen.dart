import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inner_drawer/inner_drawer.dart';
import 'package:provider/provider.dart';
import 'package:shop_app/providers/products_provider.dart';
import 'dart:io';

import './cart_screen.dart';
import '../widgets/badge.dart';
import '../providers/cart.dart';
import '../widgets/products_grid.dart';
import '../widgets/app_drawer.dart';

import '../dummy_data.dart';
import '../providers/product.dart';

class ProductsOverviewScreen extends StatefulWidget {
  //List<Product> loadedProducts = DUMMY_PRODUCTS;
  @override
  _ProductsOverviewScreenState createState() => _ProductsOverviewScreenState();
}

class _ProductsOverviewScreenState extends State<ProductsOverviewScreen> {
  var _showOnlyFavorites = false;


  @override
  void initState() {
    Provider.of<ProductsProvider>(context,listen: false).fetchProducts();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    final popUpBotton = Material(
        child: PopupMenuButton(
      onSelected: (int option) {
        setState(() {
          print(option);
          if (option == 0) {
            _showOnlyFavorites = false;
          } else
            _showOnlyFavorites = true;
        });
      },
      icon: Icon(Icons.more_vert),
      itemBuilder: (_) => [
        PopupMenuItem(
          child: Text('Show all'),
          value: 0,
        ),
        PopupMenuItem(
          child: Text('Show only Favorites'),
          value: 1,
        )
      ],
    ));

    final appBar = (Platform.isIOS)
        ? CupertinoNavigationBar(
            middle: Text('Products'),
        leading: GestureDetector(
            onTap: () => _toggle() ,
            child: Icon(Icons.menu)) ,
            trailing: Row(
              children: [
                Consumer<Cart>(
                  builder: (_, cart, child) => Badge(
                    child: GestureDetector(
                      child: child,
                      onTap: () {Navigator.of(context).pushNamed(CartScreen.nameRoute);},
                    ),
                    value: cart.itemCount.toString(),
                    color: Colors.red,
                  ),
                  child: Icon(Icons.shopping_cart),
                ),
                popUpBotton
              ],
              mainAxisSize: MainAxisSize.min,
            ))
        : AppBar(
            title: Text('Products'),
            actions: [
              popUpBotton,
              Consumer<Cart>(
                builder: (_, cart, child) => Badge(
                    child: GestureDetector(
                      child: child,
                      onTap: () {},
                    ),
                    value: cart.itemCount.toString()),
                child: Icon(Icons.shopping_cart),
              )
            ],
          );

    return (Platform.isIOS)?
    InnerDrawer(
      key: _innerDrawerKey,
      onTapClose: true, // default false
      swipe: true, // default true
      //colorTransitionChild: Color.red, // default Color.black54
      //colorTransitionScaffold: Color.black54, // default Color.black54

      //When setting the vertical offset, be sure to use only top or bottom
      offset: IDOffset.horizontal( 0.5 ),

      //scale: IDOffset.horizontal( 0.8 ), // set the offset in both directions

      // proportionalChildArea : true, // default true
      //borderRadius: 50, // default 0
      leftAnimationType: InnerDrawerAnimation.quadratic, // default static
      // backgroundDecoration: BoxDecoration(color: Colors.red ), // default  Theme.of(context).backgroundColor


      leftChild: Container(child : AppDrawer()), // required if rightChild is not set
      //rightChild: Container(), // required if leftChild is not set
      //  A Scaffold is generally used but you are free to use other widgets
      // Note: use "automaticallyImplyLeading: false" if you do not personalize "leading" of Bar
      scaffold:
        CupertinoPageScaffold(
            navigationBar: appBar,
            child: SafeArea(child: ProductsGrid(_showOnlyFavorites)),
          )
    )
        : Scaffold(
            appBar: appBar,
            body: SafeArea(child: ProductsGrid(_showOnlyFavorites)),
            drawer: AppDrawer(),
          );
  }

  final GlobalKey<InnerDrawerState> _innerDrawerKey = GlobalKey<InnerDrawerState>();

  void _toggle()
  {
    _innerDrawerKey.currentState.toggle(
      // direction is optional
      // if not set, the last direction will be used
      //InnerDrawerDirection.start OR InnerDrawerDirection.end
        direction: InnerDrawerDirection.start
    );
  }
}

import 'package:flutter/material.dart';
import 'package:shop_app/screens/product_details_screen.dart';

class ProductItemCard extends StatelessWidget {

  final String id;
  final String title;
  final String imageUrl;

  ProductItemCard({this.id,this.title,this.imageUrl});

  @override
  Widget build(BuildContext context) {
    //final mediaQuery = MediaQuery.of(context);
    return LayoutBuilder(builder:(context, constraints) =>
        Material(

          child: InkWell(

            child: Card(

              shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15)),
                elevation: 5,
                margin: EdgeInsets.only(left: 10,right: 10,top: 10),
                child: Column( children :[
                  ClipRRect(
                    child: Image.network(imageUrl, fit: BoxFit.cover, height: (constraints.maxHeight -10) * 0.8, width: double.infinity, ),
                      borderRadius: BorderRadius.only(
                        topRight: Radius.circular(15),
                        topLeft: Radius.circular(15)
                      ),),
                  Container(
                    height: (constraints.maxHeight -10) *0.2,
                    child: Padding(
                      padding: EdgeInsets.all(10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                        GestureDetector(
                        child: Icon(Icons.shopping_cart_outlined),
                      ),
                          Text(title),
                    GestureDetector(
                          child: Icon(Icons.star_border),
                        )
                        ],
                      ),
                    ),
                  )]),
               ),
              onTap: () => Navigator.of(context).pushNamed(ProductDetailsScreen.routeName ,
                  arguments: id)
            ),
        ),
//            footer: Container(
//                height: constraints.maxHeight * 0.25,
//                child : GridTileBar(
//                  backgroundColor: Colors.black38,
//                  title: Center(child: Text(title)) ,
//                  leading: GestureDetector(
//                    child: Icon(Icons.shopping_cart_outlined),
//                  ),
//                  trailing: GestureDetector(
//                    child: Icon(Icons.star_border),
//                  ),
//                )),
          );
    //,
     //   );
    //);
  }
}

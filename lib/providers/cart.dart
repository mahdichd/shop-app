import 'package:flutter/material.dart';

class CartItem {
  final String id;
  final String title;
  final double price;
  final int quantity;

  CartItem(this.id,this.title,this.price,this.quantity);
}

class Cart with ChangeNotifier{
  Map<String, CartItem> _items = { };

  Map<String, CartItem> get items{
    return {..._items};
  }

  int get itemCount {
    return _items.length;
  }

  double get totalAmount {
    var total = 0.0;
    _items.forEach((key, cartItem) {
      total+= cartItem.price * cartItem.quantity;
    });
    return total;
  }

  void clear(){
    _items = {};
    notifyListeners();
  }

  void removeItem(String id){
    _items.remove(id);
    notifyListeners();
  }

  void removeSingleItem(String id){
    if (!_items.containsKey(id)){
      return;
    }
    else if(_items[id].quantity > 1){
      _items.update(id, (item) => CartItem(item.id, item.title, item.price, item.quantity-1));
    }
    else _items.remove(id);

    notifyListeners();
  }

  void addItem(String id, String title, double price){

    if(_items.containsKey(id)){
        _items.update(id, (value) =>
            CartItem(value.id,
                value.title,
                value.price,
                value.quantity +1)
        );
    }
   else  _items.putIfAbsent(id, () => CartItem(DateTime.now().toString(), title, price,1));
   notifyListeners();
  }

}
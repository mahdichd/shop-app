import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop_app/widgets/product_item.dart';

import '../providers/products_provider.dart';
import '../widgets/product_item_card.dart';

class ProductsGrid extends StatelessWidget {
  final bool showOnlyfavorites;
  ProductsGrid(this.showOnlyfavorites);
  @override
  Widget build(BuildContext context) {
    final productsDate = Provider.of<ProductsProvider>(context);
    final products = (showOnlyfavorites == true) ? productsDate.favItems : productsDate.items;
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: GridView.builder(
        itemCount: products.length,
        itemBuilder: (context, index) {
          return ChangeNotifierProvider.value(
            value:  products[index],
            child: ProductItem(
//          id :products[index].id,
//          title: products[index].title,
//          imageUrl: products[index].imageUrl,
                ),
          );
        },
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            childAspectRatio: 10/9,
            mainAxisSpacing: 10,
            crossAxisSpacing: 10
        ),
      ),
    );
  }
}

import 'package:flutter/cupertino.dart';
import 'package:shop_app/dummy_data.dart';
import 'product.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class ProductsProvider with ChangeNotifier {
  List<Product> _items = [];
  //DUMMY_PRODUCTS

  List<Product> get items {
    return [..._items];
  }

  List<Product> get favItems{
    return _items.where((item) => item.isFavorite ).toList();
  }

  Product findById(String productId){
    return
      _items.firstWhere((product) => product.id == productId);
  }


  void toggleItemFavoriteStatus(String productId){
    findById(productId).toggleFavoriteStatus();
    notifyListeners();
  }

  Future<void> fetchProducts() async {
    const url = 'https://shop-app-2db39-default-rtdb.firebaseio.com/products.json';

    try {
      final response = await http.get(url);
      print(response.body);
      final extractedData = json.decode(response.body) as Map<String, dynamic>;
      final List<Product> loadedProducts = [];
      extractedData.forEach((prodId, prodData) {
        loadedProducts.add(Product(id: prodId,
            title: prodData['title'],
            description: prodData['description'],
            price: prodData['price'],
            imageUrl: prodData['imageUrl']));
      });

      _items = loadedProducts;
    }
    catch(error){
      print(error);
      throw error;
    }
  }
  Future<void> addProduct(Product product){
    const url = 'https://shop-app-2db39-default-rtdb.firebaseio.com/products.json';
    return http.post(url,body : json.encode({
      'title' : product.title,
      'description' : product.description,
      'price' : product.price,
      'imageUrl' : product.imageUrl,
    })).then((response) {
      print(json.decode(response.body)['name']);
      Product newProduct = Product(
          id: json.decode(response.body)['name'],
          title: product.title,
          description: product.description,
          price: product.price,
          imageUrl: product.imageUrl);
      _items.add(newProduct);
      notifyListeners();
    }).catchError((error){
      print(error);
      throw error;
    });

  }

  void updateProduct(String productId , Product newProduct){
    final productIndex = _items.indexOf(_items.firstWhere((product) => product.id == productId));
    _items[productIndex] = newProduct;
    notifyListeners();
  }

  void deleteProduct(String productId){
    _items.removeWhere((product) => product.id==productId);
    notifyListeners();
  }
}
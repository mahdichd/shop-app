import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../providers/cart.dart';
class CartItem extends StatelessWidget {
  final String id;
  final String productId;
  final String title;
  final double price;
  final int quantity;

  CartItem(this.id,this.productId,this.title,this.price,this.quantity);

  @override
  Widget build(BuildContext context) {

    return Dismissible(
      key: ValueKey(id),
      background: Container(
        margin: EdgeInsets.symmetric(horizontal: 15, vertical: 4),
        color: Theme.of(context).errorColor,
        child: Icon(Icons.delete,size: 40,color: Colors.white,),
        alignment: Alignment.centerRight,
        padding: EdgeInsets.only(right: 20),
      ),
      direction: DismissDirection.endToStart,
      confirmDismiss: (direction){
      return showDialog(context: context, builder: (context) => AlertDialog(
        title: Text('Are you sure ?'),
        content: Text('Do you want to remove the item from the cart?'),
        actions: [
            FlatButton(child: Text('No'),onPressed: (){Navigator.of(context).pop(false);},),
          FlatButton(child: Text('Yes'),onPressed: (){Navigator.of(context).pop(true);},),
        ],
      ));} ,
      onDismissed: (direction) =>  Provider.of<Cart>(context,listen: false).removeItem(productId),
      child: Card(
        margin: EdgeInsets.symmetric(horizontal: 15,
        vertical: 4),
        child: Padding(
          padding: EdgeInsets.all(8),
          child: ListTile(
            leading: CircleAvatar(child: Padding(padding: EdgeInsets.all(4.0),
            child: FittedBox(child: Text('\$$price',style: TextStyle(color: Colors.white),),),),
              backgroundColor: Theme.of(context).accentColor,

            ),
            title: Text(title,style: Theme.of(context).textTheme.headline5,),
            subtitle: Text('Total : \$${price*quantity}'),
            trailing: Text('$quantity X'),
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:shop_app/providers/products_provider.dart';
import '../providers/product.dart';
import 'package:provider/provider.dart';

class EditProductScreen extends StatefulWidget {
  static const routeName = '/edit-product-screen';

  @override
  _EditProductScreenState createState() => _EditProductScreenState();
}

class _EditProductScreenState extends State<EditProductScreen> {
  final _priceFocusNode = FocusNode();
  final _descriptionFocusNode = FocusNode();
  final _imageUrlController = TextEditingController();
  final _imageUrlFocusNode = FocusNode();
  final _form = GlobalKey<FormState>();
  var _editedProduct = Product(
    title:'',
    description: '',
    imageUrl: '',
    price: 0.0,
    id: null
  );

  var productID ;
  var _isLoading= false;
  var isInit = false;

  var formValues = {
    'id' :'',
    'title' : '',
    'description' : '',
    'price' : ''
  };

  void saveForm(){
    final isValid = _form.currentState.validate();
    if(!isValid){
      return;
    }
    _form.currentState.save();
    setState(() {
      _isLoading = true;
    });
    if(productID ==null)
      Provider.of<ProductsProvider>(context,listen: false).addProduct(_editedProduct)
    .catchError((onError){
      return showDialog(context: context, builder: (context) => AlertDialog(title: Text('An error occured!'),
      content: Text('Something went wrong'),
      actions: [
        FlatButton(child: Text('Okey'),
        onPressed: () {
          Navigator.of(context).pop();
          Navigator.of(context).pop();
        })
      ],),);
    }).then((_) {
        setState(() {
          _isLoading=false;
        });
        Navigator.of(context).pop();
      });
    else{
      Provider.of<ProductsProvider>(context,listen: false).updateProduct(productID,_editedProduct);
      setState(() {
        _isLoading=false;
      });
      Navigator.of(context).pop();
    }
    //
  }
  @override
  void initState() {
    _imageUrlFocusNode.addListener(updateImageUrl);
    super.initState();
  }

  @override
  void didChangeDependencies() {
    if(!isInit) {
       productID = ModalRoute
          .of(context)
          .settings
          .arguments as String;
      if(productID != null) {
        _editedProduct =
            Provider.of<ProductsProvider>(context).findById(productID);
        formValues = {
          'id' : _editedProduct.id,
          'title' : _editedProduct.title,
          'description' : _editedProduct.description,
          'price' : _editedProduct.price.toString()
        };
        _imageUrlController.text = _editedProduct.imageUrl;
      }
      isInit = true;

    }
    super.didChangeDependencies();
  }
  

  void updateImageUrl(){
    if(!_imageUrlFocusNode.hasFocus){
      setState(() {
        
      });
    }
  }
  @override
  void dispose() {
    _imageUrlFocusNode.removeListener(updateImageUrl);
    _priceFocusNode.dispose();
    _descriptionFocusNode.dispose();
    _imageUrlController.dispose();
    _imageUrlFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final PreferredSizeWidget appBar = (Platform.isIOS)
        ? CupertinoNavigationBar(
            middle: Text('Edit Product'),
            trailing: GestureDetector(
              child: Icon(Icons.save),
              onTap: saveForm,
            ),
          )
        : AppBar(
            title: Text('Edit Product'),
      actions: [
        IconButton(icon: Icon(Icons.save),
        onPressed: saveForm,)
      ],
          );
    final mediaQuery = MediaQuery.of(context);
    final screenHeight = mediaQuery.size.height - mediaQuery.padding.top - appBar.preferredSize.height;
    final editContent =
    (_isLoading) ?
        Center(child: CircularProgressIndicator(),)
        :
    Padding(
      padding: EdgeInsets.all(18),
      child: Form(
        key: _form,
        child: Material(
          child: ListView(
            children: [
              TextFormField(
                initialValue: formValues['title'],
                decoration: InputDecoration(labelText: 'Title'),
                textInputAction: TextInputAction.next,
                onFieldSubmitted: (_) {
                  FocusScope.of(context).requestFocus(_descriptionFocusNode);
                },
                onSaved: (value){
                  _editedProduct = Product(
                      title:value,
                      description: _editedProduct.description,
                      imageUrl: _editedProduct.imageUrl,
                      price: _editedProduct.price,
                      id: productID
                  );
                },
                validator: (value){
                  if(value.isEmpty)
                    return 'Please provide a value';
                  return null;
                },
              ),
              TextFormField(
                initialValue: formValues['description'],
                decoration: InputDecoration(labelText: 'Description'),
                focusNode: _descriptionFocusNode,
                //textInputAction: TextInputAction.next,
                keyboardType: TextInputType.multiline,
                minLines: 1,
                maxLines: 2,
                validator: (value){
                  if(value.isEmpty)
                    return 'Please provide a value';
                  return null;
                },
                onSaved: (value){
                  _editedProduct = Product(
                      title:_editedProduct.title,
                      description: value,
                      imageUrl: _editedProduct.imageUrl,
                      price: _editedProduct.price,
                      id: productID
                  );
                },
//              onFieldSubmitted: (_){
//                FocusScope.of(context).requestFocus(_priceFocusNode);
//              },
              ),
              TextFormField(
                initialValue: formValues['price'],
                decoration: InputDecoration(labelText: 'Price'),
                textInputAction: TextInputAction.next,
                keyboardType: TextInputType.number,
                validator: (value){
                  if(value.isEmpty)
                    return 'Please provide a value';
                  if(double.tryParse(value) == null)
                    return 'Please enter a valid number';
                  if(double.parse(value) <=0)
                    return 'Please enter a value greater than 0';
                  return null;
                },
                onSaved: (value){
                  _editedProduct = Product(
                      title:_editedProduct.title,
                      description: _editedProduct.description,
                      imageUrl: _editedProduct.imageUrl,
                      price: double.parse(value),
                      id: productID
                  );
                },
                //focusNode: _priceFocusNode,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[

                  Container(
                    child: Expanded(
                      child: TextFormField(
                        decoration: InputDecoration(labelText: 'Image URL'),
                        keyboardType: TextInputType.url,
                        textInputAction: TextInputAction.done,
                        focusNode: _imageUrlFocusNode,
                        controller: _imageUrlController,
                        onSaved: (value){
                          _editedProduct = Product(
                              title:_editedProduct.title,
                              description: _editedProduct.description,
                              imageUrl: value,
                              price: _editedProduct.price,
                              id: productID
                          );
                        },
                        validator: (value){
                          if(value.isEmpty)
                            return 'Please provide a value';
                          if(!value.startsWith('http'))
                            return 'Please enter a valid Url';
                          if(!value.endsWith('.png') && !value.endsWith('.jpg') && !value.endsWith('.jpeg') )
                            return 'Pleae enter a valid image Url';
                          return null;
                        },
                        onEditingComplete: () {
                          setState(() {});
                        },
                        onFieldSubmitted: (_){
                          FocusScope.of(context).requestFocus(FocusNode());
                          saveForm();
                        },
                      ),
                    ),
                  ),Container(
                    width: screenHeight * 0.15,
                    height: screenHeight * 0.15,
                    margin: EdgeInsets.only(
                      top: 8,
                      left: 10,
                    ),
                    decoration: BoxDecoration(
                      border: Border.all(
                        width: 1,
                        color: Colors.grey,
                      ),
                    ),
                    child: _imageUrlController.text.isEmpty
                        ? Text('Enter a URL')
                        : FittedBox(
                      child: Image.network(
                        _imageUrlController.text,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
    return (Platform.isIOS)
        ? CupertinoPageScaffold(
            navigationBar: appBar,
            child: editContent,
          )
        : Scaffold(
            appBar: appBar,
            body: editContent,
          );
  }
}

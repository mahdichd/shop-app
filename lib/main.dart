import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:shop_app/widgets/user_product_item.dart';

import './screens/edit_product_screen.dart';
import './providers/cart.dart';
import './providers/products_provider.dart';
import './screens/product_details_screen.dart';
import './screens/products_overview_screen.dart';
import './screens/cart_screen.dart';
import './providers/orders.dart';
import './screens/orders_screen.dart';
import './screens/user_products_screen.dart';


void main() {
  runApp(MyApp());

}

class MyApp extends StatelessWidget {

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(providers: [
        ChangeNotifierProvider(
        create: (context) => ProductsProvider()),
        ChangeNotifierProvider(
          create: (context) => Cart()),
        ChangeNotifierProvider(
          create: (context) => Orders()),
    ],
      child: MaterialApp(
        title: 'Shop App',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          accentColor: Colors.purple,
            textTheme: ThemeData.light().textTheme.copyWith(
                headline6: TextStyle(
                    fontSize: 20,
                    fontFamily: 'Acme'
                ),
              headline5: TextStyle(
                  fontFamily: 'Castoro',
                  fontSize: 19),
            )
        ),
        home: ProductsOverviewScreen(),
        routes: {
          ProductDetailsScreen.routeName : (ctx) => ProductDetailsScreen(),
          CartScreen.nameRoute : (ctx) => CartScreen(),
          OrdersScreen.nameRoute : (ctx) => OrdersScreen(),
          UserProductsScreen.routeName : (ctx) => UserProductsScreen(),
          EditProductScreen.routeName : (ctx) => EditProductScreen(),
        },
      ),
    );
   //  );
  }
}


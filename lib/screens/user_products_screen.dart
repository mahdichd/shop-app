import 'package:flutter/material.dart';
import 'package:flutter_inner_drawer/inner_drawer.dart';
import 'package:provider/provider.dart';
import 'package:shop_app/providers/products_provider.dart';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import '../providers/products_provider.dart';
import '../widgets/user_product_item.dart';
import '../widgets/app_drawer.dart';
import './edit_product_screen.dart';

class UserProductsScreen extends StatelessWidget {
  static const routeName = '/user-products';

  @override
  Widget build(BuildContext context) {
    final productsData = Provider.of<ProductsProvider>(context);

    final PreferredSizeWidget appBar =
    (Platform.isIOS) ?
    CupertinoNavigationBar(
      middle: Text('Your Products'),
    trailing: GestureDetector(child: Icon(Icons.add), onTap: (){
      Navigator.of(context).pushNamed(EditProductScreen.routeName);
    },),
        leading: GestureDetector(
        onTap: () => _toggle() ,
    child: Icon(Icons.menu)),)
        : AppBar(
      title: Text('Your Products'),
      actions: <Widget>[
      IconButton(
        icon: const Icon(Icons.add),
        onPressed: () {
          Navigator.of(context).pushNamed(EditProductScreen.routeName);
        },
      ),
    ],);

    final content = Padding(
      padding: EdgeInsets.all(8),
      child: ListView.builder(
        itemCount: productsData.items.length,
        itemBuilder: (_, i) => Column(
          children: [
            UserProductItem(
              productsData.items[i].id,
              productsData.items[i].title,
              productsData.items[i].imageUrl,
            ),
            Divider(),
          ],
        ),
      ),
    );
    return
      (Platform.isIOS) ?

      InnerDrawer(
          key: _innerDrawerKey,
          onTapClose: true, // default false
          swipe: true, // default true
          //colorTransitionChild: Color.red, // default Color.black54
          //colorTransitionScaffold: Color.black54, // default Color.black54

          //When setting the vertical offset, be sure to use only top or bottom
          offset: IDOffset.horizontal( 0.5 ),

          //scale: IDOffset.horizontal( 0.8 ), // set the offset in both directions

          // proportionalChildArea : true, // default true
          //borderRadius: 50, // default 0
          leftAnimationType: InnerDrawerAnimation.quadratic, // default static
          // backgroundDecoration: BoxDecoration(color: Colors.red ), // default  Theme.of(context).backgroundColor


          leftChild: Container(child : AppDrawer()), // required if rightChild is not set
          //rightChild: Container(), // required if leftChild is not set
          //  A Scaffold is generally used but you are free to use other widgets
          // Note: use "automaticallyImplyLeading: false" if you do not personalize "leading" of Bar
          scaffold:
          CupertinoPageScaffold(
            navigationBar: appBar,
            child: content,
          )
    )
      :
      Scaffold(
      appBar: AppBar(
        title: const Text('Your Products'),

      ),
      drawer: AppDrawer(),
      body:content ,
    );
  }
  final GlobalKey<InnerDrawerState> _innerDrawerKey = GlobalKey<InnerDrawerState>();

  void _toggle()
  {
    _innerDrawerKey.currentState.toggle(
      // direction is optional
      // if not set, the last direction will be used
      //InnerDrawerDirection.start OR InnerDrawerDirection.end
        direction: InnerDrawerDirection.start
    );
  }
}

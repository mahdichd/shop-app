import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop_app/providers/cart.dart';

import '../screens/product_details_screen.dart';
import '../providers/product.dart';

class ProductItem extends StatelessWidget {

//  final String id;
//  final String title;
//  final String imageUrl;
//
//  ProductItem({this.id,this.title,this.imageUrl});

  @override
  Widget build(BuildContext context) {
    final product = Provider.of<Product>(context, listen: false);
    final cart = Provider.of<Cart>(context, listen: false);
    return LayoutBuilder(builder:(context, constraints) =>
    ClipRRect(
      borderRadius: BorderRadius.circular(15),
      child: GridTile(

        child: GestureDetector(
            child: Image.network(product.imageUrl, fit: BoxFit.cover),
            onTap: () => Navigator.of(context).pushNamed(ProductDetailsScreen.routeName ,
            arguments: product.id),) ,
        footer: Container(
          height: constraints.maxHeight * 0.25,
            child : GridTileBar(
          backgroundColor: Colors.black38,
          title: Center(child: Text(product.title)) ,
          leading: GestureDetector(
            child: Icon(Icons.shopping_cart_outlined, color: Colors.red,),
            onTap: () {cart.addItem(product.id, product.title, product.price);
              Flushbar(
                message: 'Added item to cart!',
                // Even the button can be styled to your heart's content
                mainButton: FlatButton(
                  child: Text(
                    'UNDO',
                    style: TextStyle(color: Theme.of(context).errorColor),
                  ),
                  onPressed: () {
                    cart.removeSingleItem(product.id);
                  },
                ),
                duration: Duration(milliseconds: 1500),
                // Show it with a cascading operator
              )..show(context);
            },
          ),
          trailing: Consumer<Product>(
            builder: (context, value, _) => GestureDetector(
              child: Icon(
                (product.isFavorite) ? Icons.star : Icons.star_border,
                color: Colors.red,),
              onTap: product.toggleFavoriteStatus,
            ),
          ),
        )),
      ),
    )
    );
  }
}

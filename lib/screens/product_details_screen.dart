import 'dart:ui';

import 'package:flutter/material.dart';
import 'dart:io';
import 'package:provider/provider.dart';
import 'package:flutter/cupertino.dart';

import '../providers/cart.dart';
import '../providers/products_provider.dart';

class ProductDetailsScreen extends StatelessWidget {

  static final routeName = '/product_details_screen';

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);

    final String productId = ModalRoute.of(context).settings.arguments as String;
    final cart = Provider.of<Cart>(context,listen: false);
    final productsProvider   = Provider.of<ProductsProvider>(context,listen: false);
    final loadedProduct = productsProvider.findById(productId);
    final PreferredSizeWidget appBar =
    (Platform.isIOS) ?
    CupertinoNavigationBar(
      middle: Text(loadedProduct.title),)
        : AppBar(
      title: Text(loadedProduct.title),);
    final screenSize = mediaQuery.size.height - mediaQuery.padding.top - appBar.preferredSize.height;
    final detailsContent = Column(
      children: [
        ClipRRect(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(20),
            bottomRight: Radius.circular(20)
          ),
          child: Image.network(loadedProduct.imageUrl,height: screenSize * 0.4,width: double.infinity, fit: BoxFit.cover,),
        ),
//        SizedBox(
//          height: screenSize * 0.07,
//        ),
        Container(
          margin: EdgeInsets.only(left: 10,right: 10),
          height: screenSize * 0.08,
          child: Center(child: Text(loadedProduct.title,style: Theme.of(context).textTheme.headline6,)),
        ),
//        SizedBox(
//          height: screenSize * 0.07,
//        ),
        Container(
          margin: EdgeInsets.only(left: 20,right: 20),
          height: screenSize * 0.08,
          child: Center(child: Text(loadedProduct.description,style: Theme.of(context).textTheme.headline5,textAlign: TextAlign.center,)),
        ),
        Container(
          margin: EdgeInsets.only(left: 20,right: 20),
          height: screenSize * 0.08,
          child: Center(child: Text('\$ ${loadedProduct.price}',style: Theme.of(context).textTheme.headline5,)),
        ),
        Spacer(),
        Container(
          height: screenSize * 0.1,
            margin: EdgeInsets.only(left: 20,right: 20,bottom: 20),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Container(
                  decoration: BoxDecoration(color : Colors.white,borderRadius:  BorderRadius.circular(10)),
                    height: screenSize * 0.05,
                    child: Material(
                      child:   Consumer<ProductsProvider>(
    builder: (context, value, _) => GestureDetector(
                        child: Icon(
                          (loadedProduct.isFavorite) ? Icons.star : Icons.star_border,
                          color: Colors.purple,),
                        onTap: () => productsProvider.toggleItemFavoriteStatus(productId) ,
                      )),
                    )

                ),
                Flexible(
                      child: FlatButton(
                        child: Container(child: Center(child: Text('ADD TO CART',style: TextStyle(color: Theme.of(context).accentTextTheme.headline6.color))),
                        decoration:  BoxDecoration(color: Theme.of(context).accentColor,
                        borderRadius: BorderRadius.circular(30)),
                        width: double.infinity,
                        height: screenSize * 0.05,),
                        onPressed: () => cart.addItem(loadedProduct.id, loadedProduct.title, loadedProduct.price),
                      ),

                  ),

              ],
            ),
          ),

      ],
    );

    return
      (Platform.isIOS)?
      CupertinoPageScaffold(
        navigationBar: appBar ,
        child: SafeArea(child: detailsContent) ,
      )
          : Scaffold(
        appBar: appBar ,
        body: SafeArea(child:detailsContent ),
      );
  }
}

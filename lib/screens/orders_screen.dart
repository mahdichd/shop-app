import 'package:flutter/material.dart';
import 'package:flutter_inner_drawer/inner_drawer.dart';
import 'package:provider/provider.dart';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:shop_app/widgets/app_drawer.dart';

import '../providers/orders.dart' show Orders;
import '../widgets/order_item.dart';

class OrdersScreen extends StatelessWidget {
  static final nameRoute = '/orders-screen';
  @override
  Widget build(BuildContext context) {
    final orderData = Provider.of<Orders>(context);
    final PreferredSizeWidget appBar =
    (Platform.isIOS) ?
    CupertinoNavigationBar(
      middle: Text('Your Orders'),
      leading: GestureDetector(
          onTap: () => _toggle() ,
          child: Icon(Icons.menu)) ,)
        : AppBar(
      title: Text('Your Orders'),);

    final ordersContent = ListView.builder(
      itemCount: orderData.orders.length,
      itemBuilder: (ctx, i) => OrderItem(orderData.orders[i]),
    );
    return
    (Platform.isIOS) ?
    InnerDrawer(
        key: _innerDrawerKey,
        onTapClose: true, // default false
        swipe: true, // default true
        //colorTransitionChild: Color.red, // default Color.black54
        //colorTransitionScaffold: Color.black54, // default Color.black54

        //When setting the vertical offset, be sure to use only top or bottom
        offset: IDOffset.horizontal( 0.5 ),

        //scale: IDOffset.horizontal( 0.8 ), // set the offset in both directions

        // proportionalChildArea : true, // default true
        //borderRadius: 50, // default 0
        leftAnimationType: InnerDrawerAnimation.quadratic, // default static
        // backgroundDecoration: BoxDecoration(color: Colors.red ), // default  Theme.of(context).backgroundColor


        leftChild: Container(child : AppDrawer()), // required if rightChild is not set
        //rightChild: Container(), // required if leftChild is not set
        //  A Scaffold is generally used but you are free to use other widgets
        // Note: use "automaticallyImplyLeading: false" if you do not personalize "leading" of Bar
        scaffold:
     CupertinoPageScaffold(
      navigationBar: appBar,
      child: ordersContent
    )
    )
    :
    Scaffold(
      appBar: appBar,
      body: ordersContent,
      drawer: AppDrawer(),
    );
  }
  final GlobalKey<InnerDrawerState> _innerDrawerKey = GlobalKey<InnerDrawerState>();

  void _toggle()
  {
    _innerDrawerKey.currentState.toggle(
      // direction is optional
      // if not set, the last direction will be used
      //InnerDrawerDirection.start OR InnerDrawerDirection.end
        direction: InnerDrawerDirection.start
    );
  }
}

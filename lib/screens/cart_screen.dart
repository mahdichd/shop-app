import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../providers/orders.dart';
import '../providers/cart.dart' show Cart;
import 'dart:io';

import '../widgets/cart_item.dart';
class CartScreen extends StatelessWidget {
  static final nameRoute ='/cart-screen';
  final String title  = 'Your Cart';
  @override
  Widget build(BuildContext context) {
    final cart = Provider.of<Cart>(context,);
    final appBar =
    (Platform.isIOS) ?
        CupertinoNavigationBar(
          middle: Text(title),
        )
        : AppBar(title: Text(title),);

    final cartContent =
        Column(
          children: [
            Card(
              margin: EdgeInsets.all(15),
              child: Padding(
                padding: EdgeInsets.all(8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: Text('Total',
                      style: TextStyle(fontSize: 18, fontFamily: 'NoticiaText'),),
                    ),
                    Spacer(),
//                    Flexible(
//                      fit: FlexFit.loose,
//                      child:
                      Chip(label: Text('\$${cart.totalAmount}', style: TextStyle(color: Theme.of(context).accentTextTheme.headline6.color),),
                      backgroundColor: Theme.of(context).accentColor,),
                    //),
//                    Flexible(
//                      fit: FlexFit.loose,
//                      child:
                      FlatButton( child :Text('ORDER NOW',style: TextStyle(color:Theme.of(context).accentColor,fontSize: 18,fontFamily: 'NoticiaText' ),),
                      onPressed: (){Provider.of<Orders>(context,listen: false).addOrder(cart.items.values.toList(), cart.totalAmount);
                      cart.clear();},)
                   // )
                  ],
                ),
              ),
            ),
            SizedBox(height: 10,),
            Expanded(child: ListView.builder(
              itemCount: cart.items.length,
              itemBuilder: (context, index) => CartItem(cart.items.values.toList()[index].id,
    cart.items.keys.toList()[index],
                  cart.items.values.toList()[index].title,
                  cart.items.values.toList()[index].price,
                  cart.items.values.toList()[index].quantity),
            ),)
          ],
        );
    return  (Platform.isIOS) ?
        CupertinoPageScaffold(
          navigationBar: appBar,
          child: SafeArea(child: cartContent),
        )
        : Scaffold(appBar: appBar,
    body: cartContent,);
  }
}
